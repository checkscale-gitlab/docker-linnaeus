#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
            set -- php-fpm "$@"
fi

if [ "$(whoami)" = "root" ]; then
    mkdir -p /cache
    /bin/chown www-data:www-data /cache
fi

exec "$@"
