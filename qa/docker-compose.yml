version: "3.8"

services:
  traefik:
    image: registry.gitlab.com/naturalis/bii/linnaeus/docker-linnaeus/traefik:${IMAGE_VERSION:?Variable IMAGE_VERSION is empty}
    command:
      - --global.sendAnonymousUsage=false
      - --providers.docker.exposedByDefault=false
      - --providers.docker.endpoint=tcp://docker-proxy:2375
      - --providers.docker.network=docker-proxy
      - --entrypoints.web.address=:80
      - --entrypoints.web.http.redirections.entryPoint.to=websecure
      - --entrypoints.websecure.address=:443
    depends_on:
      - nginx
      - docker-proxy
    networks:
      - web
      - docker-proxy
    ports:
      - 80:80
      - 443:443

  docker-proxy:
    image: registry.gitlab.com/naturalis/bii/linnaeus/docker-linnaeus/docker-proxy:${IMAGE_VERSION:?}
    environment:
      CONTAINERS: 1
    networks:
      - docker-proxy
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro

  nginx:
    image: registry.gitlab.com/naturalis/bii/linnaeus/docker-linnaeus/nginx:${IMAGE_VERSION:?}
    depends_on:
      - php-fpm
    networks:
      - web
    labels:
      - traefik.enable=true
      - traefik.http.routers.nginx.entrypoints=websecure
      - traefik.http.routers.nginx.tls=true
      - traefik.http.routers.nginx.rule=hostregexp(`{host:.+}`)
    volumes:
      - ./test-app:/var/www:ro
      - ./nginx:/etc/nginx/conf.d:ro

  php-fpm:
    image: registry.gitlab.com/naturalis/bii/linnaeus/docker-linnaeus/php-fpm:${IMAGE_VERSION:?}
    environment:
      MYSQL_DATABASE: develop
      MYSQL_USER: develop
      MYSQL_PASSWORD: develop
    depends_on:
      - mariadb
    networks:
      - web
      - backend
    volumes:
      - ./test-app:/var/www:ro
      - type: tmpfs
        target: /tmp
        tmpfs:
          # 10MB
          size: 10485760

  mariadb:
    image: registry.gitlab.com/naturalis/bii/linnaeus/docker-linnaeus/mariadb:${IMAGE_VERSION:?}
    environment:
      MYSQL_ROOT_PASSWORD: root
      MYSQL_DATABASE: develop
      MYSQL_USER: develop
      MYSQL_PASSWORD: develop
    networks:
      - backend
    volumes:
      - ./database/storage:/var/lib/mysql:rw

networks:
  web:
    internal: false
  backend:
    internal: true
  docker-proxy:
    internal: true
